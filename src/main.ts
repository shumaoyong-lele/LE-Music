import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import QiiUI from 'qii-ui'
import 'qii-ui/dist/index.css'

const app = createApp(App)

app.use(QiiUI)

app.use(createPinia())
app.use(router)

app.mount('#app')
